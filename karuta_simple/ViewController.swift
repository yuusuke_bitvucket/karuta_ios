//
//  ViewController.swift
//  karuta_simple
//
//  Created by yuusuke on 2018/12/09.
//  Copyright © 2018 yuusuke. All rights reserved.
//

import UIKit
import AVFoundation


class ViewController: UIViewController, AVAudioRecorderDelegate, AVAudioPlayerDelegate  {

    @IBOutlet weak var tapButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var indexCountLabel: UILabel!
    @IBOutlet weak var allCount: UILabel!
    
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    var isRecording = false
    var isPlaying = false

    var objectList: Array! = []
    var index: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        indexCountLabel.text = String("1")
        allCount.text = String("48")
        tapButton.setTitle("スタート", for: .normal)

        //0から48までのリストを作成し、シャッフルする。
        for str in 0..<48 {
            objectList.append(String(str))
        }
        objectList.shuffle()
        
        dump(objectList)
        

    }

    @IBAction func onRecord(_ sender: Any) {
        if !isRecording {
            
            let session = AVAudioSession.sharedInstance()
            try! session.setCategory(AVAudioSession.Category.playAndRecord,
                                     mode: AVAudioSession.Mode.default,
                                     options: AVAudioSession.CategoryOptions.defaultToSpeaker)
            try! session.setActive(true)
            
            let settings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 44100,
                AVNumberOfChannelsKey: 2,
                AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
            ]
            
            audioRecorder = try! AVAudioRecorder(url: getRecordingURL(), settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            
            isRecording = true
            
            recordButton.setTitle("STOP", for: .normal)
            
        }else{
            
            audioRecorder.stop()
            isRecording = false
            
            recordButton.setTitle("RECORD", for: .normal)
            
        }
    }
    
    @IBAction func onPlay(_ sender: Any) {
        
        if isPlaying {
            audioPlayer.stop()
            isPlaying = false
        }
        
        if index == 48 {
            tapButton.setTitle("おしまい", for: .normal)
            index += 1
            return
        }
        else if index == 49
        {
            tapButton.setTitle("もういっかい？", for: .normal)
            index += 1
            return
        }
        else if index > 49 {
            tapButton.setTitle("スタート", for: .normal)
            objectList.shuffle()
            dump(objectList)
            index = 0
            indexCountLabel.text = String(index + 1)
            return
        }
        
        
        //ファイル名を作成する。
        playIndex()
        
        recordButton.isEnabled = false
        indexCountLabel.text = String(index + 1)
        tapButton.setTitle("つぎへ", for: .normal)
        index += 1
    }

    func playIndex() {
        //再生する
        let value = objectList[index]
        audioPlayer = try! AVAudioPlayer(contentsOf: getURL(value as! String))
        audioPlayer.delegate = self
        audioPlayer.play()
        
        isPlaying = true

    }
    
    func getRecordingURL() -> URL{
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let docsDirect = paths[0]
        let name: String = textField.text!
        let url = docsDirect.appendingPathComponent("karuta" + name + ".m4a")
        return url
    }

    func getURL(_ index: String) -> URL{
        // サウンドファイルのパスを生成
        let string: String = String("RecordData/karuta" + index)
        let soundFilePath = Bundle.main.path(forResource: string , ofType: ".m4a")!
        let sound:URL = URL(fileURLWithPath: soundFilePath)

        return sound
    }

    @IBAction func onBack(_ sender: Any) {
        
        if isPlaying {
            audioPlayer.stop()
            isPlaying = false
        }
        
        if index == 1 {
            index = 0
            objectList.shuffle()
            tapButton.setTitle("スタート", for: .normal)
        }
            
        else if index > 1{
            index -= 1
            indexCountLabel.text = String(index)
        }
            /*
        else if index == 1{
            tapButton.setTitle("スタート", for: .normal)
            index = 0
            indexCountLabel.text = String(index + 1)
           // return
        }
        else{
            index -= 1
            indexCountLabel.text = String(index)
            //return
        }
 */


        
    }
}

